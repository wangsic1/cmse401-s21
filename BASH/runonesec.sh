#!/bin/bash

$1 &
PID=$!

( sleep 5; kill $PID) &
WPID=$!

wait $PID
kill $WPID
